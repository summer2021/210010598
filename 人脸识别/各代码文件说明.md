# dataset2：人脸数据集
# trainer:训练好的模型
# 00_faceDetection.py:测试是否能检测到人脸
# 01_dataset.py：制作数据集
# 02_training.py:训练模型
# 03_recognition.py:人脸识别
# STWhite.jpg：测试摄像头
# haarcascade_frontalface_default.xml:opencv预训练级联分类器
# img.jpg：测试摄像头
# save_pic.py：保存一张图片
import cv2
import numpy as np
import os 
import time
import subprocess
 
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('trainer/trainer2.yml') #Trained model path
cascadePath = "haarcascade_frontalface_default.xml"  #Cascaded pre-trained classifier path
faceCascade = cv2.CascadeClassifier(cascadePath);

font = cv2.FONT_HERSHEY_SIMPLEX
#iniciate id counter
id = 0 
names = ['CMX', 'AOBAMA','STWhite' , 'NONE'] 
# Initialize and start realtime video capture
cam = cv2.VideoCapture(0)
cam.set(3, 640) # set video widht
cam.set(4, 480) # set video height
 
# Define min window size to be recognized as a face
minW = 0.1*cam.get(3)
minH = 0.1*cam.get(4)

#tumble detect
fg = cv2.createBackgroundSubtractorMOG2()
scale = 0
hline = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 4), (-1, -1)) #define the convolution kernel
vline = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 1), (-1, -1))
while True:
    time.sleep(0.02)
    ret, img =cam.read()
    #img = cv2.flip(img, -1) # Flip vertically
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
     
    faces = faceCascade.detectMultiScale( 
        gray,
        scaleFactor = 1.2,
        minNeighbors = 5,
        minSize = (int(minW), int(minH)),
       )
 
    for(x,y,w,h) in faces:
        id, confidence = recognizer.predict(gray[y:y+h,x:x+w])
        # Check if confidence is less then 100 ==> "0" is perfect match 
        if (confidence < 100):
            id = names[id]
            confidence = "  {0}%".format(round(100 - confidence))
            print(id)
            print(confidence)
        else:
            id = "unknown"
            confidence = "  {0}%".format(round(100 - confidence))
            print(id)
            print(confidence)
            subprocess.call("/root/led",shell=True)    #buzzer alarm
            subprocess.call("/usr/bin/python3 /root/led",shell=True) #SMS_alarm
             
    #tumble detect
    #canny
    image= img.copy()
    blurred = cv2.GaussianBlur(image, (3, 3), 0)
    gray = cv2.cvtColor(blurred, cv2.COLOR_RGB2GRAY)
    xgrad = cv2.Sobel(gray, cv2.CV_16SC1, 1, 0) #x
    ygrad = cv2.Sobel(gray, cv2.CV_16SC1, 0, 1) #y
    edge_output = cv2.Canny(xgrad, ygrad, 50, 150)

    fgmask = fg.apply(edge_output)
    #closed operation
    #hline = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 4), (-1, -1)) #
    #vline = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 1), (-1, -1))
    result = cv2.morphologyEx(fgmask,cv2.MORPH_CLOSE,hline)
    result = cv2.morphologyEx(result,cv2.MORPH_CLOSE,vline)

    #inflation
    dilateim = cv2.dilate(result,cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)),iterations=1)
    dilateim = cv2.dilate(dilateim,cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)),iterations=1)

    #find contour
    contours, hier = cv2.findContours(dilateim, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    for c in contours:
            if cv2.contourArea(c) > 4000:
                    (x,y,w,h) = cv2.boundingRect(c)
                    #if scale==0:scale=-1;break
                    scale = w/h

    #if scale >0 and scale <1:
    #       print("walking")
    if scale > 2:
            print("falled")
            subprocess.call("/root/led",shell=True) #buzzer alarm
            subprocess.call("/usr/bin/python3 /root/bin/exam6.py", shell = True) #SMS alarm
    scale = 0
 
# Do a bit of cleanup
#print("\n [INFO] Exiting Program and cleanup stuff")
#cam.release()
#cv2.destroyAllWindows()
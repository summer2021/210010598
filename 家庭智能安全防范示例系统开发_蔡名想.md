## 前言

初次接触到开源项目还是2020年12月的时候，当时gitee发起了一个叫《开源指北》的项目，意在编写一本开源入门电子书籍，我当时也是一个新手，只好尽我的能力去搜集了点资料，最终审核人员采纳了我的PR，虽然我的工作很细微，但是能得到认可却是一件值得高兴的事情，这为我开启了开源的大门。后来，在2021年3月至5月末，我自学了《鸟哥的Linux私房菜基础篇》第四版，这使我有了一定的Linux知识和操作基础。非常幸运能够在暑期2021活动中承担一个项目，在提交项目申请书前，有提前做了一些调查，并且结合自己的经验，构想了一个比较合理的方案，包括选用什么传感器，用什么算法等等。

## 项目目标

家庭安全防控是与人们的人身财产安全息息相关的重要问题。本项目希望在树莓派平台上实现以下居家情形的安全防范或告警：

- 人意外摔倒的情况。
- 可燃气体泄露警示。
- 入侵检测，识别到未经验证的人脸。    
  一旦出现上述任意一种安全情景，系统通过蜂鸣器发声和短信方式自动报警。

## 项目开发过程

该项目需要考虑三个场景的报警，因此在实际开发中，我把大的项目拆分成了五个模块，分别是蜂鸣器模块、短信模块、烟雾检测模块、人脸识别模块、摔倒检测模块。下面分别介绍一下每个模块：

- 蜂鸣器模块：有源蜂鸣器，高电平触发，只需要给I/O引脚高电平即可，用于发出声音报警。

  ![蜂鸣器](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/%E8%9C%82%E9%B8%A3%E5%99%A8.png)

- 短信模块：即是SIM900A模块，需要插入移动或联通手机卡，采用5V1A的电源适配器供电，树莓派通过串口向短信模块发送指令，短信模块再发短信到指定手机号码上，用于短信报警。

  ![SIM900A](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/SIM900A.png)

- 烟雾检测模块：使用MQ2烟雾传感器，该传感器使用的气敏材料是在清洁空气中电导率较低的二氧化锡（SnO2），当传感器所处环境中存在可燃气体时，传感器模块内部电路的电导率随空气中可燃气体浓度的增大而增大，浓度达到阈值时，传感器模块的相关I/O引脚输出有效电平（低电平）。树莓派通过检测相关引脚的电平变化即可获取燃气泄漏的信息，并通过蜂鸣器模块和短信模块发出警报。

  ![MQ2](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/MQ2.png)

- 人脸识别模块：使用OpenCV进行实时人脸识别，OpenCV库提供了三种人脸识别算法，分别是EigenFishfaces、FisherFaces和LBPH，最终选用LBPH（Local Binary Patterns Histogram，局部二值模式直方图）。其主要思想是以当前点与其领域像素的相对变化作为处理结果，因此在图像灰度整体发生变化（单调变化），从LBPH提取的特征能保持不变。

  如何使用？

  用cv2.face.LBPHFaceRecognizer_create()生成LBPH识别器实例模型

  ![人脸1](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/%E4%BA%BA%E8%84%B81.png)

  用recognizer.train()训练模型

  ![人脸2](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/%E4%BA%BA%E8%84%B82.png)

  用recognizer.predict()推理，预测人脸，返回ID和置信度

![人脸3](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/%E4%BA%BA%E8%84%B83.png)

- 摔倒检测模块：使用背景差分法，用新的图像减去背景就可以得到前景，原理如下图：

  ![摔倒1](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/%E6%91%94%E5%80%921.png)

  OpenCV库包含三种易于使用的背景差分法，分别是BackgroundSubtractorMOG、BackgroundSubtractorMOG2和BackgroundSubtractorGMG，我采用的BackgroundSubtractorMOG2是以高斯混合模型为基础的背景/前景分割算法，这个算法的特点是为每一个像素选择一个合适数目的高斯分布，它能够更好地适应光照不同等各种场景，并且可以检测影子。在实际应用中，我用背景差分法提取摔倒的人体，再通过腐蚀、膨胀等形态学处理，提取得到人体轮廓，用最小外接矩形框住人体，计算矩形的宽长比即可判断是否摔倒。

  ## 项目框架

  ![整体框架](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/%E6%95%B4%E4%BD%93%E6%A1%86%E6%9E%B6.png)

摄像头和MQ2烟雾传感器模块用于采集信息，树莓派openEuler作为信息处理中心，对输入信息进行处理，最后由蜂鸣器和SIM900A短信模块来进行动作响应。

处理逻辑如下图：

![处理逻辑](https://gitee.com/daaaaaaaaaaa/drawing-bed/raw/master/%E5%A4%84%E7%90%86%E9%80%BB%E8%BE%91.png)

## 项目成果

该项目所有代码、说明文档、展示视频已上传至gitee项目仓库，[仓库地址](https://gitee.com/openeuler-competition/summer2021-108/tree/master)。

## 项目开发体会

- 项目本身意义：该项目考虑的是家庭安全问题，陌生人闯入、意外摔倒、发生火情都是生活中常见的安全问题，任意一种情况发生都会对用户造成巨大的危害，这个项目能够在一定程度上帮助解决用户的痛点问题，及时报警，具有现实意义。 
- 个人总结：技术是一种工具，能够利用各种工具手段去解决问题，提高生活质量，这令我感到非常满意，也是我开发该项目的动力之一。在开发过程中，得到了主办方中科院和 openEuler社区的资金、技术大力支持，以及和魏建刚老师建立了良好顺畅的沟通渠道，这次活动让我不仅学习到了新知识，还了解到openEuler开源社区的运作方式。最后，非常感激能够参与到此次开源软件供应链点亮计划中来，开源，刚刚开始。
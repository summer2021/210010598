import cv2
import numpy as np
import time
import subprocess

cam = cv2.VideoCapture(0) 
cam.set(3, 640) # set video widht
cam.set(4, 480) # set video height
scale=0

fg = cv2.createBackgroundSubtractorMOG2()
hline = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 4), (-1, -1)) #define the convolution kernel
vline = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 1), (-1, -1))
while True:
	time.sleep(0.02)
	ret,img = cam.read()
	
	#canny
	image= img.copy()
	blurred = cv2.GaussianBlur(image, (3, 3), 0)
	gray = cv2.cvtColor(blurred, cv2.COLOR_RGB2GRAY)
	xgrad = cv2.Sobel(gray, cv2.CV_16SC1, 1, 0) #x
	ygrad = cv2.Sobel(gray, cv2.CV_16SC1, 0, 1) #y
	edge_output = cv2.Canny(xgrad, ygrad, 50, 150)
	
	fgmask = fg.apply(edge_output)
	#closed operation
	result = cv2.morphologyEx(fgmask,cv2.MORPH_CLOSE,hline)
	result = cv2.morphologyEx(result,cv2.MORPH_CLOSE,vline)

	#inflation
	dilateim = cv2.dilate(result,cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)),iterations=1)
	dilateim = cv2.dilate(dilateim,cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)),iterations=1)
	
	#find contour
	contours, hier = cv2.findContours(dilateim, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
	for c in contours:
		if cv2.contourArea(c) > 4000:
			(x,y,w,h) = cv2.boundingRect(c)
			scale = w/h
	
	#if scale >0 and scale <1:
	#	print("walking")
	if scale > 2:
		print("falled")
		subprocess.call("/root/led",shell=True)
		#print(retcode)
	scale = 0

# python
对于python，通过标准库中的subprocess包来fork一个子进程，并运行一个外部的程序。subprocess包中定义了多个创建子进程的函数，以下介绍其中三种。
## 1.subprocess.call()
父进程等待子进程完成
返回退出信息(returncode，相当于Linux exit code)
## 2.subprocess.check_call()
父进程等待子进程完成
返回0
检查退出信息，如果returncode不为0，则举出错误subprocess.CalledProcessError，该对象包含有returncode属性，可用try…except…来检查。
## 3.subprocess.check_output()
父进程等待子进程完成
返回子进程向标准输出的输出结果
检查退出信息，如果returncode不为0，则举出错误subprocess.CalledProcessError，该对象包含有returncode属性和output属性，output属性为标准输出的输出结果，可用try…except…来检查。

## 这里我用到第一种，用法如下：

```
import subprocess
subprocess.call("/root/led",shell=True)
```

> 注意这里命令要用绝对路径，命令的执行和PATH变量有关，不同的shell有不同的PATH变量。


# C
这里介绍一种方法
- 头文件
#include<stdlib.h> 
- 定义函数 
int system(const char * string); 
- 过程：system()会调用fork()产生子进程，由子进程来调用/bin/sh-cstring来执行参数string字符串所代表的命令，此命令执行完后随即返回原调用的进程。 
- 范例：

```
#include<stdlib.h> 
void main(void)
{
    system(“ls -al /etc/passwd”);
}

```
